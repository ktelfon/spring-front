import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CreateUserComponent } from './create-user/create-user.component';
import { ItemListComponent } from './item-list/item-list.component';
import { LoginComponent } from './login/login.component';
import { AuthGaurdService } from './service/auth-guard.service';
import { UpdateUserComponent } from './update-user/update-user.component';
import { UserListComponent } from './user-list/user-list.component';

const routes: Routes = [
  {
    path: 'users',
    component: UserListComponent,
    canActivate: [AuthGaurdService],
  },
  {
    path: 'create',
    component: CreateUserComponent,
    canActivate: [AuthGaurdService],
  },
  {
    path: 'update/:id',
    component: UpdateUserComponent,
    canActivate: [AuthGaurdService],
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'items',
    component: ItemListComponent,
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
