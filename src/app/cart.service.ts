import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class CartService {
  private addItemPath = 'http://localhost:8080/add/';

  constructor(private http: HttpClient) {}

  addItem(id: any) {
    return this.http.get(this.addItemPath + id);
  }
}
