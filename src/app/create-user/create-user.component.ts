import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { User } from '../User';
import { UserService } from '../user.service';

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  user:User;

  constructor(
    private userService: UserService,
    private router:Router) {
    this.user = new User();
   }

   onSubmit(){
     this.userService
     .save(this.user)
     .subscribe(()=>{
       this.router.navigate(['/users']);
     });
   }

  ngOnInit(): void {
  }



}
