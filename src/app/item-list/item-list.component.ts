import { Component, OnInit } from '@angular/core';
import { CartService } from '../cart.service';
import { Item } from '../item';
import { ItemService } from '../item.service';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.css']
})
export class ItemListComponent implements OnInit {

  items:Item[];
  condition = false;

  constructor(private itemService: ItemService, private cartService : CartService) { }

  ngOnInit(): void {
    this.itemService.findAll().subscribe(

    (data) => {        this.items = data;      }

    );
  }

  addItem(id){
    this.cartService.addItem(id).subscribe(()=>{
      this.condition = true;
      setTimeout(() => {
        this.condition = false;
     }, 3000)
    });
  }

}
