import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Item } from './item';

@Injectable({
  providedIn: 'root'
})
export class ItemService {
 
  private allItemsPath = "http://localhost:8080/items";

  constructor(private http: HttpClient) { }

  findAll() {
    return this.http.get<Item[]>(this.allItemsPath);
  }
}
