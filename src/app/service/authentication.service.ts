import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

export class User {
  constructor(public status: string) {}
}

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private httpClient: HttpClient) {}

  authenticate(username, password) {
    const headers = new HttpHeaders({
      Authorization: 'Basic ' + btoa(username + ':' + password),
    });

    let promise = new Promise((resolve, reject)=>{

      this.httpClient
      .get<string>('http://localhost:8080/user/current', { headers })
      .subscribe(
        (currentUser) => {
          console.log(currentUser);
          sessionStorage.setItem('username', currentUser);
          let authString = 'Basic ' + btoa(username + ':' + password);
          sessionStorage.setItem('basicauth', authString);
          resolve();
        }
      );

    });
    
    return promise;
  }

  isUserLoggedIn() {
    let user = sessionStorage.getItem('username');
    return !(user === null);
  }

  logOut() {
    sessionStorage.removeItem('username');
  }
}
