import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { User } from '../User';
import { UserService } from '../user.service';

@Component({
  selector: 'app-update-user',
  templateUrl: './update-user.component.html',
  styleUrls: ['./update-user.component.css']
})
export class UpdateUserComponent implements OnInit {

  user:User;

  constructor(
    private userService: UserService,
    private route: ActivatedRoute,
    private router:Router) {
      this.userService.findById(
        this.route.snapshot.paramMap.get('id')
      ).
      subscribe(
        (data)=>{
          this.user = data;
        }
      );
   }

   onSubmit(){
     this.userService
     .save(this.user)
     .subscribe(()=>{
       this.router.navigate(['/users']);
     });
   }

  ngOnInit(): void {
  }


}
