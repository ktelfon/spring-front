import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './User';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  private url: string;
  private postUrl: string;
  private findByIdPath: string;

  constructor(private http: HttpClient) {
    this.url = 'http://localhost:8080/user/all';
    this.postUrl = 'http://localhost:8080/user';
    this.findByIdPath = 'http://localhost:8080/user/';
  }

  public findAll(): Observable<User[]> {
    return this.http.get<User[]>(this.url);
  }

  public save(user: User){
    return this.http.post<User>(this.postUrl, user);
  }

  public findById(id: string): Observable<User>{
    return this.http.get<User>(this.findByIdPath+id);
  }
}
